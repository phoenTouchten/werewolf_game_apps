﻿using UnityEngine;
using System.Collections;

public class ViewController : MonoBehaviour {

	private tk2dUIItem[] buttons;
	protected Transform myTransform;
	
	#region Mono
	protected virtual void Awake () {
		myTransform = transform;
		GetViewButtons();
	}
		
	protected virtual void Start () {}
	
	protected virtual void OnEnable () {
		EnableListener();
		EnableButton(true);
	}
	
	protected virtual void OnDisable () {
		DisableListener();
		EnableButton(false);
	}
	#endregion
	
	#region Listener
	protected virtual void EnableListener () {
		foreach (tk2dUIItem btn in buttons) {
			btn.OnUpUIItem += OnButtonUp;
		}
	}
	
	protected virtual void DisableListener () {
		foreach (tk2dUIItem btn in buttons) {
			btn.OnUpUIItem -= OnButtonUp;
		}
	}
	#endregion
	
	#region Input
	private void GetViewButtons () {
		buttons = gameObject.GetComponentsInChildren<tk2dUIItem>();	
	}
	
	private void EnableButton (bool state) {
		foreach (tk2dUIItem btn in buttons) {
			btn.enabled = state;
		}
	}
	#endregion
	
	#region Event
	protected virtual void OnButtonUp (tk2dUIItem obj) {}
	#endregion
}
