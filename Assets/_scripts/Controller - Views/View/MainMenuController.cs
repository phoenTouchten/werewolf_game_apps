using UnityEngine;
using System.Collections;

public class MainMenuController : ViewController {
	
	private const string BtnRuleBook = "btnRuleBook";
	private const string BtnCharacterBook = "btnCharacterBook";
	private const string BtnStartGame = "btnStartGame";
	
	#region Event
	protected override void OnButtonUp (tk2dUIItem obj)
	{
		base.OnButtonUp (obj);
		
		switch (obj.name) {
		case BtnRuleBook :
			MainEventsManager.OnChangeScene(ViewName.View_RuleBook);
			break;
		case BtnCharacterBook :
			MainEventsManager.OnChangeScene(ViewName.View_CharacterBook);
			break;
		case BtnStartGame :
			MainEventsManager.OnChangeScene(ViewName.View_PreGame);
			break;
		}
	}
	#endregion
}
