using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameplayTest : ViewController {

	private const string BtnNext = "btnNext";
	private const string BtnMainMenu = "btnMainMenu";
	
	private const string AnchorPlayerPath = "AnchorPlayer";
	private Transform anchor;
	
	private const string PlayerCardPath = "PlayerCard";
	
	public int playerCount;
	public int werewolfCount;
	private const int guardianCount = 1;
	private const int seerCount = 1;
	private const int narratorCount = 1;
	public int villagerCount;
	
	private List<Player> players;

	private string[] names = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S"};
	
	private const string namePath = "Name";
	private const string rolePath = "Role";
	private const float positionIncrement = .35f;
	
#region Mono	
	
	protected override void Awake ()
	{
		base.Awake ();
		anchor = myTransform.FindChild(AnchorPlayerPath);
		players = new List<Player>();
	}
	
	protected override void Start ()
	{
		base.Start ();
		CreatePlayers();
		DisplayPlayers();
	}
	
#endregion
	
	
	private void CreatePlayers () {
		villagerCount = playerCount - werewolfCount - narratorCount;
		
		for (int i = 0; i < playerCount; i++) {
			Player player = new Player(i, names[i]);
			players.Add(player);
		}
		
		// random narrator
		int nCount = 0;
		do {
			int randomIndex = Random.Range(0, playerCount);
			if (players[randomIndex].role == Role.Villager) {
				players[randomIndex].role = Role.Narrator;
				nCount++;
			}
		} while (nCount < narratorCount);
		
		// random werewolf
		int wCount = 0;
		do {
			int randomIndex = Random.Range(0, playerCount);
			if (players[randomIndex].role == Role.Villager) {
				players[randomIndex].role = Role.Werewolf;
				wCount++;
			}
		} while (wCount < werewolfCount);
		
		// random guardian
		int gCount = 0;
		do {
			int randomIndex = Random.Range(0, playerCount);
			if (players[randomIndex].role == Role.Villager) {
				players[randomIndex].role = Role.Guardian;
				gCount++;
			}
		} while (gCount < guardianCount);
		
		// random seer
		int sCount = 0;
		do {
			int randomIndex = Random.Range(0, playerCount);
			if (players[randomIndex].role == Role.Villager) {
				players[randomIndex].role = Role.Seer;
				sCount++;
			}
		} while (sCount < seerCount);
	}
	
	private void DisplayPlayers () {
		for (int i = 0; i < playerCount; i++) {
			GameObject go = Instantiate(Resources.Load(PlayerCardPath)) as GameObject;
			Transform goTransform = go.transform;
			goTransform.parent = anchor;
			goTransform.localPosition = Vector3.down * positionIncrement * i;
			
			tk2dTextMesh name = goTransform.FindChild(namePath).GetComponent<tk2dTextMesh>();
			tk2dTextMesh role = goTransform.FindChild(rolePath).GetComponent<tk2dTextMesh>();
			Utility.ChangeTextMesh(name, players[i].name);
			Utility.ChangeTextMesh(role, players[i].role.ToString());
		}
	}
	
	
	
#region Event
	
	protected override void OnButtonUp (tk2dUIItem obj)
	{
		base.OnButtonUp (obj);
		switch (obj.name) {
		case BtnNext :
			
			break;
		case BtnMainMenu :
			MainEventsManager.OnChangeScene(ViewName.View_MainMenu);
			break;
		}
	}
	
#endregion
}