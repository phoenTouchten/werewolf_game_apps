﻿using UnityEngine;
using System.Collections;

public class PreGameController : ViewController {

	private const string BtnMainMenu = "btnMainMenu";
	private const string BtnPlay = "btnPlay";
	
	#region Event
	protected override void OnButtonUp (tk2dUIItem obj)
	{
		base.OnButtonUp (obj);
		
		switch (obj.name) {
		case BtnMainMenu :
			MainEventsManager.OnChangeScene(ViewName.View_MainMenu);
			break;
		case BtnPlay :
			MainEventsManager.OnChangeScene(ViewName.View_Game);
			break;
		}
	}
	#endregion
}
