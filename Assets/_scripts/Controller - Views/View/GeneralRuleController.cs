﻿using UnityEngine;
using System.Collections;

public class GeneralRuleController : ViewController {

	private const string BtnMainMenu = "btnMainMenu";
	
	#region Event
	protected override void OnButtonUp (tk2dUIItem obj)
	{
		base.OnButtonUp (obj);
		
		switch (obj.name) {
		case BtnMainMenu :
			MainEventsManager.OnChangeScene(ViewName.View_MainMenu);
			break;
		}
	}
	#endregion
}
