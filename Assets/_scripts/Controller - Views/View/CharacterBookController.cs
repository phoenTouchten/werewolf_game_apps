﻿using UnityEngine;
using System.Collections;

public class CharacterBookController : ViewController {
	
	private const string BtnMainMenu = "btnMainMenu";
	private const string BtnNext = "btnNext";
	private const string BtnPrev = "btnPrev";
	private const float SpawnPointIncrement = 3;
	private const float offsetY = .5f;
	private const int initialActiveCardNumber = 1;
	
	private const string anchorPath = "Anchor (MiddleCenter)/CardGroup";
	private Transform anchor;
	
	private int activeCardNumber;
	private enum Direction { LEFT, RIGHT }
	
	#region Mono
	protected override void Awake ()
	{
		base.Awake ();
		anchor = transform.FindChild(anchorPath);
		GameData.Load();
	}
	
	protected override void Start ()
	{
		base.Start ();
		LoadCharacter();
		activeCardNumber = initialActiveCardNumber;
	}
	#endregion
	
	#region Core Logic	
	private void LoadCharacter () {
		Debug.Log(GameData.current.characters.Length);
		for (int i = 0; i < GameData.current.characters.Length; i++) {
			GameObject go = CharacterCard.SpawnCharacterCard(	GameData.current.characters[i].name, 
																GameData.current.characters[i].imageName,
																GameData.current.characters[i].description);
			Transform goTransform = go.transform;
			goTransform.parent = anchor;
			goTransform.localPosition = new Vector3(i*SpawnPointIncrement, offsetY, 0);
		}
	}
	
	private void MoveCardGroup (Direction dir) {
		switch (dir) {
		case Direction.LEFT :
			if (activeCardNumber < GameData.current.characters.Length) {
				anchor.Translate(Vector3.left * SpawnPointIncrement);
				activeCardNumber++;
			}
			break;
		case Direction.RIGHT :
			if (activeCardNumber > initialActiveCardNumber) {
				anchor.Translate(Vector3.right * SpawnPointIncrement);
				activeCardNumber--;
			}
			break;
		}
		Debug.Log(activeCardNumber);
	}
	#endregion
	
	#region Event
	protected override void OnButtonUp (tk2dUIItem obj)
	{
		base.OnButtonUp (obj);
		
		switch (obj.name) {
		case BtnMainMenu :
			MainEventsManager.OnChangeScene(ViewName.View_MainMenu);
			break;
		case BtnNext :
			MoveCardGroup(Direction.LEFT);
			break;
		case BtnPrev :
			MoveCardGroup(Direction.RIGHT);
			break;
		}
	}
	#endregion
	
	
}
