using UnityEngine;
using System.Collections;

public static class Utility {

	static public void ChangeTextMesh (tk2dTextMesh textMesh, string message) {
		textMesh.text = message;
		textMesh.Commit();
	}
}
