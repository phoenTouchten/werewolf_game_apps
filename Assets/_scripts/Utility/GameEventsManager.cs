﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameEventsManager {
	
	#region GAME
	
	#endregion
	
	#region UI
	
	public delegate void UpdateUIHandler (Hashtable hash);
	public static event UpdateUIHandler onUpdateUIEvent;
	
	public static void UpdateUI (Hashtable hash) {
		if (onUpdateUIEvent != null)
			onUpdateUIEvent(hash);
	}
	
	
	public delegate void PlayersEventHandler (List<Player> players);
	public static event PlayersEventHandler onDisplayPlayerListEvent;
	
	public static void DisplayPlayerList (List<Player> players) {
		if (onDisplayPlayerListEvent != null)
			onDisplayPlayerListEvent(players);
	}
	
	#endregion
}
