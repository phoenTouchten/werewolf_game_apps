﻿using UnityEngine;
using System.Collections;

public class MainEventsManager {
	
	public delegate void ViewChangerHandler (ViewName view);
	public static event ViewChangerHandler onChangeViewEvent;
	
	public static void OnChangeScene (ViewName view) {
		if (onChangeViewEvent != null)
			onChangeViewEvent(view);
	}
}
