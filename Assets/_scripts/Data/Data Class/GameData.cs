using UnityEngine;
using System.Collections;

public class GameData {

	static public GameData current;
	private const string path = "Game Data";
	
	// Define the XML structure
	public class Character {
		public string name;
		public string imageName;
		public string description;
	}
	public Character[] characters;
	
	public class Phase {
		public string backgroundImageName;
		public string cardImageName;
		public string narationText;
	}
	public Phase[] phases;
	
	static public void Load () {
		if(current == null)
		{
			current = (GameData)XmlManager.LoadInstanceAsXml(path, typeof(GameData));
		}
	}	
}
