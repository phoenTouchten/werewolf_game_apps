﻿using UnityEngine;
using System.Collections;

public class Player {
	public int id;
	public string name;
	public Role role;
	
	public Player (int id, string name) {
		this.id = id;
		this.name = name;
		this.role = Role.Villager;
	}
}

public enum Role {
	Werewolf,
	Villager,
	Guardian,
	Seer,
	Narrator,
	Ghost
}