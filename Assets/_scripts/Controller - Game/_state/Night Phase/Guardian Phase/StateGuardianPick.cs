using UnityEngine;
using System.Collections;

[System.Serializable]
public class StateGuardianPick : GameState {

	private const string BtnNext = "btnNext";
	private const string BtnMainMenu = "btnMainMenu";
	private const string BtnPlayerCard = "PlayerCard";
	
	private const string narrationText = "Guardian, choose someone to protect.";
	private const string backgroundImage = "night";
	private const string cardImage = "guardian_may";
	
	private const int MaxPick = 1;
	private int pickCount = 0;
	
	private GameObject btnNextGO;
	
	public override void OnEnter ()
	{
		base.OnEnter ();
		GetPickButton();
		btnNextGO.SetActive(false);
	}
	
	private void GetPickButton () {
		foreach (tk2dUIItem btn in buttons) {
			if (btn.name.Equals(BtnNext)) {
				btnNextGO = btn.gameObject;	
			}
		}	
	}
	
	protected override void ReferenceUIobject ()
	{
		base.ReferenceUIobject ();
		UIobject = gameController.UIplayerList;
	}
	
	protected override void ChangeUI ()
	{
		base.ChangeUI ();
		
		Hashtable hash = new Hashtable();
		hash.Add(UInarrativeElement.NarrationText, narrationText);
		hash.Add(UInarrativeElement.BackgroundImage, backgroundImage);
		hash.Add(UInarrativeElement.CardImage, cardImage);
		GameEventsManager.UpdateUI(hash);
	}
	
	protected override void OnButtonUp (tk2dUIItem obj)
	{
		base.OnButtonUp (obj);
		switch (obj.name) {
		case BtnNext :
			OnNextPressed();
			break;
		case BtnMainMenu :
			MainEventsManager.OnChangeScene(ViewName.View_MainMenu);
			break;
		}
		
		switch (obj.tag) {
		case BtnPlayerCard :
			OnPlayerPicked(obj);
			break;
		}
	}
	
	private void OnNextPressed () {
		gameController.GoToState(gameController.s_guardianClose);
	}
	
	private void OnPlayerPicked (tk2dUIItem obj) {
		int playerIndex = int.Parse(obj.name.TrimStart(BtnPlayerCard.ToCharArray()));
		PlayerCardUIController playerCard = obj.GetComponent<PlayerCardUIController>();
		switch (playerCard.CardState) {
		case true :
			if (pickCount < 0) break;
			pickCount--;
			playerCard.CardState = false;
			ClearGuardedPlayer();
			break;
		case false :
			if (pickCount >= MaxPick) break;
			pickCount++;
			playerCard.CardState = true;
			GetGuardedPlayer(playerIndex);
			break;
		}
		
		EnablePickButton();
	}
	
	private void ClearGuardedPlayer () {
		gameController.guardedPlayer = gameController.placeholderPlayer;	
	}
	
	private void GetGuardedPlayer (int index) {
		gameController.guardedPlayer = gameController.players[index];	
		Debug.Log(gameController.guardedPlayer.name);
	}
	
	private void EnablePickButton () {
		if (pickCount == MaxPick) {
			btnNextGO.SetActive(true);	
		}
		else {
			btnNextGO.SetActive(false);
		}	
	}
}