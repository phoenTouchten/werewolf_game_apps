using UnityEngine;
using System.Collections;

[System.Serializable]
public class StateWerewolfWon : GameState {

	private const string BtnNext = "btnNext";
	private const string BtnMainMenu = "btnMainMenu";
	
	private const string narrationText = "WEREWOLF WON!!!";
	private const string backgroundImage = "day";
	private const string cardImage = "werewolf_2";
	
	protected override void ChangeUI ()
	{
		base.ChangeUI ();
		
		Hashtable hash = new Hashtable();
		hash.Add(UInarrativeElement.NarrationText, narrationText);
		hash.Add(UInarrativeElement.BackgroundImage, backgroundImage);
		hash.Add(UInarrativeElement.CardImage, cardImage);
		GameEventsManager.UpdateUI(hash);
	}
	
	protected override void OnButtonUp (tk2dUIItem obj)
	{
		base.OnButtonUp (obj);
		switch (obj.name) {
		case BtnNext :
			OnNextPressed();
			break;
		case BtnMainMenu :
			MainEventsManager.OnChangeScene(ViewName.View_MainMenu);
			break;
		}
	}
	
	private void OnNextPressed () {}
}
