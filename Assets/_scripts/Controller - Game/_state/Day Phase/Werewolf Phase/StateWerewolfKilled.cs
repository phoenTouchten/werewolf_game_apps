using UnityEngine;
using System.Collections;

[System.Serializable]
public class StateWerewolfKilled : GameState {

	private const string BtnNext = "btnNext";
	private const string BtnMainMenu = "btnMainMenu";
	
	private const string narrationText = "The werewolf has been slained";
	private const string backgroundImage = "day";
	private const string cardImage = "werewolf_2";
	
	public override void OnEnter ()
	{
		gameController.werewolfCount--;
		base.OnEnter ();
	}
	
	protected override void ChangeUI ()
	{
		base.ChangeUI ();
		
		Hashtable hash = new Hashtable();
		hash.Add(UInarrativeElement.NarrationText, narrationText);
		hash.Add(UInarrativeElement.BackgroundImage, backgroundImage);
		hash.Add(UInarrativeElement.CardImage, cardImage);
		GameEventsManager.UpdateUI(hash);
	}
	
	protected override void OnButtonUp (tk2dUIItem obj)
	{
		base.OnButtonUp (obj);
		switch (obj.name) {
		case BtnNext :
			OnNextPressed();
			break;
		case BtnMainMenu :
			MainEventsManager.OnChangeScene(ViewName.View_MainMenu);
			break;
		}
	}
	
	private void OnNextPressed () {
		if (gameController.werewolfCount <= 0) {	
			gameController.GoToState(gameController.s_villagerWon);
		}
		else {
			gameController.GoToState(gameController.s_villagerSearch);	
		}
	}
}
