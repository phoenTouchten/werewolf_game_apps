using UnityEngine;
using System.Collections;

[System.Serializable]
public class StateVillagerWrong : GameState {

	private const string BtnNext = "btnNext";
	private const string BtnMainMenu = "btnMainMenu";
	
	private const string narrationText = "Villagers, you have lynched the wrong person";
	private const string backgroundImage = "day";
	private const string cardImage = "child_george";
	
	protected override void ChangeUI ()
	{
		base.ChangeUI ();
		
		Hashtable hash = new Hashtable();
		hash.Add(UInarrativeElement.NarrationText, narrationText);
		hash.Add(UInarrativeElement.BackgroundImage, backgroundImage);
		hash.Add(UInarrativeElement.CardImage, cardImage);
		GameEventsManager.UpdateUI(hash);
	}
	
	protected override void OnButtonUp (tk2dUIItem obj)
	{
		base.OnButtonUp (obj);
		switch (obj.name) {
		case BtnNext :
			OnNextPressed();
			break;
		case BtnMainMenu :
			MainEventsManager.OnChangeScene(ViewName.View_MainMenu);
			break;
		}
	}
	
	private void OnNextPressed () {
		Debug.Log(gameController.werewolfCount);
		Debug.Log(gameController.villagerCount);
		if (gameController.werewolfCount >= gameController.villagerCount) {	
			gameController.GoToState(gameController.s_werewolfWon);	
		}
		else {
			gameController.GoToState(gameController.s_night);
		}
	}
}
