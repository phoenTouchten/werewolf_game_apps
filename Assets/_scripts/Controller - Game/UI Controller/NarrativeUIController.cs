﻿using UnityEngine;
using System.Collections;

public class NarrativeUIController : MonoBehaviour, IGameView {
	
	private const string PlaceholderImage = "placeholder";
	private const string PlaceholderNarration = "Placeholder Text";
	
	private const string narationPath = "Body/backgroundText/txtDescription";
	private const string bgImagePath = "Body/backgroundImage/sprtBackground";
	private const string cardImagePath = "Body/cardImage/sprtCard";
	
	private Transform myTransform;
	private tk2dTextMesh tmNarration;
	private tk2dSprite sprtBackgroundImage;
	private tk2dSprite sprtCardImage;
	
	// Debug
	private const string DebugGhostTextPath = "Debug/txtGhost";
	private const string DebugGuardianTextPath = "Debug/txtGuardian"; 
	private const string DebugSeerTextPath = "Debug/txtSeer"; 
	private const string DebugVillagerTextPath = "Debug/txtVillager"; 
	private const string DebugWerewolfTextPath = "Debug/txtWerewolf"; 
	
	private tk2dTextMesh tmGhostCount;
	private tk2dTextMesh tmGuardianCount;
	private tk2dTextMesh tmSeerCount;
	private tk2dTextMesh tmVillagerCount;
	private tk2dTextMesh tmWolfCount;
	
	#region Mono
	
	private void Awake () {
		GetReferences();
	}
	
	private void OnEnable () {
		GameEventsManager.onUpdateUIEvent += UpdateUI;
	}
	
	private void OnDisable () {
		GameEventsManager.onUpdateUIEvent -= UpdateUI;
	}
	
	#endregion
	
	
	#region UI
	
	public void UpdateUI (Hashtable hash) {
		Utility.ChangeTextMesh(tmNarration, (string)hash[UInarrativeElement.NarrationText]);
		sprtBackgroundImage.SetSprite((string)hash[UInarrativeElement.BackgroundImage]);
		sprtCardImage.SetSprite((string)hash[UInarrativeElement.CardImage]);
	}
	
	#endregion
	
	
	#region Reference 
	
	private void GetReferences () {
		myTransform = transform;
		
		tmNarration = myTransform.FindChild(narationPath).GetComponent<tk2dTextMesh>();
		sprtBackgroundImage = myTransform.FindChild(bgImagePath).GetComponent<tk2dSprite>();
		sprtCardImage = myTransform.FindChild(cardImagePath).GetComponent<tk2dSprite>();
		
		tmGhostCount = myTransform.FindChild(DebugGhostTextPath).GetComponent<tk2dTextMesh>();
		tmGuardianCount = myTransform.FindChild(DebugGuardianTextPath).GetComponent<tk2dTextMesh>();
		tmSeerCount = myTransform.FindChild(DebugSeerTextPath).GetComponent<tk2dTextMesh>();
		tmVillagerCount = myTransform.FindChild(DebugVillagerTextPath).GetComponent<tk2dTextMesh>();
		tmWolfCount = myTransform.FindChild(DebugWerewolfTextPath).GetComponent<tk2dTextMesh>();
	}
	
	#endregion
}

public enum UInarrativeElement {
	NarrationText,
	BackgroundImage,
	CardImage,
	GhostCount,
	GuardianCount,
	SeerCount,
	VillagerCount,
	WerewolfCount
}
