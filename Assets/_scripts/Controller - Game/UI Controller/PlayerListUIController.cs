﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerListUIController : MonoBehaviour, IGameView {

	private Transform myTransform;
	
	private const string AnchorPlayerPath = "Players";
	private Transform anchor;
	
	private const string narationPath = "Body/backgroundText/txtDescription";
	private const string cardImagePath = "Body/cardImage/sprtCard";
	
	private tk2dTextMesh tmNarration;
	private tk2dSprite sprtCardImage;
	
	private const string PlayerCardPath = "PlayerCard";
	private const string PlayerCardTag = "PlayerCard";
	
	private const float verticalIncrement = .35f;
	private const float horizontalIncrement = 1f;
	private Vector2 positionIncrement;
	
	private const int MaxVerticalList = 7;
	
	#region Mono	
	
	private void Awake ()
	{
		myTransform = transform;
		
		anchor = myTransform.FindChild(AnchorPlayerPath);
		
		tmNarration = myTransform.FindChild(narationPath).GetComponent<tk2dTextMesh>();
		sprtCardImage = myTransform.FindChild(cardImagePath).GetComponent<tk2dSprite>();
		
		positionIncrement = new Vector2(horizontalIncrement, verticalIncrement);
	}
	
	private void OnEnable () {
		Debug.Log("asdasdasdas");
		GameEventsManager.onDisplayPlayerListEvent += DisplayPlayerList;
		GameEventsManager.onUpdateUIEvent += UpdateUI;
	}
	
	private void OnDisable () {
		GameEventsManager.onDisplayPlayerListEvent -= DisplayPlayerList;
		GameEventsManager.onUpdateUIEvent -= UpdateUI;
	}
	
	#endregion
	
	#region CORE
	
	private void DisplayPlayerList (List<Player> players) 
	{
		Debug.Log("sdsdsd");
		int row = 0;
		int column = 0;
		
		for (int i = 0; i < players.Count; i++) 
		{	
			GameObject go = Instantiate(Resources.Load(PlayerCardPath)) as GameObject;
			Transform goTransform = go.transform;
			goTransform.name = PlayerCardPath + i.ToString();
				
			PlayerCardUIController playerCard = goTransform.GetComponent<PlayerCardUIController>();
			playerCard.SetCard(players[i].name, players[i].role);
			
			goTransform.parent = anchor;
			goTransform.localPosition = (Vector3.right * positionIncrement.x *  column) + (Vector3.down * positionIncrement.y * row);
			row++;
			if (row == MaxVerticalList) {
				column++;
				row = 0;
			}
		}
	}
	
	#endregion
	
	#region UI
	
	public void UpdateUI (Hashtable hash) {
		Utility.ChangeTextMesh(tmNarration, (string)hash[UInarrativeElement.NarrationText]);
		sprtCardImage.SetSprite((string)hash[UInarrativeElement.CardImage]);
	}
	
	#endregion
}
