﻿using UnityEngine;
using System.Collections;

public class PlayerCardUIController : MonoBehaviour {
	
	private const string namePath = "Name";
	private const string rolePath = "Role";
	private const string offGOpath = "backgroundOff";
	private const string onGOpath = "backgroundOn";
	
	private tk2dTextMesh _name;
	private tk2dTextMesh _role;	
	private GameObject offGO;
	private GameObject onGo;
	
	private bool cardState;
	
	private void Awake () 
	{
		Transform myTransform = transform;
		
		_name = myTransform.FindChild(namePath).GetComponent<tk2dTextMesh>();
		_role = myTransform.FindChild(rolePath).GetComponent<tk2dTextMesh>();	
		offGO = myTransform.FindChild(offGOpath).gameObject;
		onGo = myTransform.FindChild(onGOpath).gameObject;
		
		CardState = false;
	}
	
	public void SetCard (string name, Role role) 
	{	
		Utility.ChangeTextMesh(_name, name);
		Utility.ChangeTextMesh(_role, role.ToString());	
	}
	
	public bool CardState {
		get {
			return this.cardState;	
		}
		set {
			cardState = value;
			onGo.SetActive(value);
			offGO.SetActive(!value);
		}
	}
}
