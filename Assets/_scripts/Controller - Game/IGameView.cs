﻿using UnityEngine;
using System.Collections;

public interface IGameView {
	void UpdateUI(Hashtable hash);
}
