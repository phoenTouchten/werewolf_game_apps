﻿using UnityEngine;
using System.Collections;

public class GameState : FSMState {
	[System.NonSerialized] public GameController gameController;		
	
	protected tk2dUIItem[] buttons;
	protected GameObject UIobject;
	
	#region State Machine
	
	public override void OnEnter ()
	{
		base.OnEnter ();
		ReferenceUIobject();
		SetUIobject(true);
		GameEventsManager.DisplayPlayerList(gameController.players);
		GetButtons();
		EnableListener();
		ChangeUI();
	}
	
	public override void OnExit ()
	{
		base.OnExit ();
		DisableListener();
		SetUIobject(false);
	}

	#endregion
	
	#region Listener
	
	protected virtual void EnableListener () {
		foreach (tk2dUIItem btn in buttons) {
			btn.OnUpUIItem += OnButtonUp;
		}
	}
	
	protected virtual void DisableListener () {
		foreach (tk2dUIItem btn in buttons) {
			btn.OnUpUIItem -= OnButtonUp;
		}
	}
	
	#endregion
	
	
	#region Buttons
	
	protected virtual void GetButtons () {
		buttons = gameController.GetComponentsInChildren<tk2dUIItem>();	
	}
	
	protected virtual void EnableButton (bool state) {
		foreach (tk2dUIItem btn in buttons) {
			btn.enabled = state;
		}
	}
	
	protected virtual void OnButtonUp (tk2dUIItem obj) {}
	
	#endregion
	
	#region UI
	
	protected virtual void ChangeUI () {}
	
	private void SetUIobject (bool state) {
		UIobject.SetActive(state);	
	}
	
	protected virtual void ReferenceUIobject () {
		UIobject = gameController.UInarrative;
	}	
	
	#endregion
}
