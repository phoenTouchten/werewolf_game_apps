using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum ViewName {
	View_MainMenu,
	View_RuleBook,
	View_CharacterBook,
	View_PreGame,
	View_Game
}

public class MainController : MonoBehaviour {

	private List<GameObject> _views;
	private GameObject _lastView;
	private GameObject _currentView;
		
	#region Mono
	private void Awake () {
		MakeViews();
	}
	
	private void Start () {
		GameData.Load();
		
		// disable all view
		foreach (GameObject v in _views) {
			v.SetActive(false);
		}
		StartViewStateMachine();
	}
	
	private void OnEnable () {
		EnableListener();	
	}
	
	private void OnDisable () {
		DisableListener();
	}
	#endregion
	
	
	#region Listener
	private void EnableListener () {
		MainEventsManager.onChangeViewEvent += OnChangeScene;
	}
	
	private void DisableListener () {
		MainEventsManager.onChangeViewEvent -= OnChangeScene;
	}
	#endregion
	
	
	#region Core Logic
	private void MakeViews () {
		if (_views == null) {
			_views = new List<GameObject>();
		}
		
		foreach (ViewName _view in System.Enum.GetValues(typeof(ViewName))) {
			GameObject go = Instantiate(Resources.Load(_view.ToString())) as GameObject;
			go.name = _view.ToString();
			_views.Add(go);
		}
	}
	
	private void StartViewStateMachine () {
		_currentView = _views[0];
		_lastView = _currentView;
		ActivateView();
	}
	
	private void ActivateView () {
		_lastView.SetActive(false);
		_currentView.SetActive(true);
	}
	#endregion
	
	
	#region Event
	private void OnChangeScene (ViewName view) {
		GameObject go;
		go = _views.Find(v => v.name.Equals(view.ToString() , System.StringComparison.CurrentCultureIgnoreCase));
		
		_lastView = _currentView;
		_currentView = go;
		
		ActivateView();
		
		Debug.Log("Last : " + LastView);
		Debug.Log("Current : " + CurrentView);
	}
	#endregion
	
	
	#region Properties
	public ViewName CurrentView {
		get {
			string name = _currentView.name;
			ViewName view = (ViewName)System.Enum.Parse(typeof(ViewName), name);
			return view;
		}
	}

	public ViewName LastView {
		get {
			string name = _lastView.name;
			ViewName view = (ViewName)System.Enum.Parse(typeof(ViewName), name);
			return view;
		}
	}
	#endregion
}