using UnityEngine;
using System.Collections;

public static class CharacterCard {
	
	private const string path = "CharacterCardTemplate";
	private const string namePath = "txtName";
	private const string imagePath = "txtImage";
	private const string descPath = "txtDescription";
	private const string cardPath = "imgCard";
	
	public static GameObject SpawnCharacterCard (string name, string image, string desc) {
		
		GameObject go;
		tk2dTextMesh _name;
		tk2dTextMesh _image;
		tk2dTextMesh _description;
		tk2dSprite _card;
		
		go = GameObject.Instantiate(Resources.Load(path)) as GameObject;
		Transform goTransform = go.transform;
		_name = goTransform.FindChild(namePath).GetComponent<tk2dTextMesh>();
		_image = goTransform.FindChild(imagePath).GetComponent<tk2dTextMesh>();
		_description = goTransform.FindChild(descPath).GetComponent<tk2dTextMesh>();
		_card = goTransform.FindChild(cardPath).GetComponent<tk2dSprite>();
		
		Utility.ChangeTextMesh(_name, name);
		Utility.ChangeTextMesh(_image, image);
		Utility.ChangeTextMesh(_description, desc);
		
		_card.SetSprite(image);
		
		return go;
	}
}
